﻿$(document).ready(function () {
    var formDOM = $("#form");

    if (formDOM.length !== 0) {
        form();
    }

    $('#pricing-carousel').on('slid.bs.carousel', function (e) {
        var index = $('#pricing-carousel .item.active').data("index");

        if (index === 2 && $(window).width() < 768) {
            $(".ribbon.mobile").fadeIn();
        }
    });

    $('#pricing-carousel').on('slide.bs.carousel', function (e) {
        var index = $('#pricing-carousel .item.active').data("index");

        if (index === 2) {
            $(".ribbon.mobile").fadeOut();
        }
    });

    $(window).resize(function () {
        var index = $('#pricing-carousel .item.active').data("index");

        if ($(window).width() >= 768) {
            $(".ribbon.mobile").hide();
        }

        if ($(window).width() < 768 && index === 2) {
            $(".ribbon.mobile").show();
        }
    })

});



var form = function () {
    //Inputs
    const submit = document.querySelector("#button");
    const nameField = document.querySelector("#Name");
    const emailField = document.querySelector("#Email");
    const emailHelp = document.querySelector("#emailHelp");

    //Create a validty class
    class CheckValidity {
        constructor(input, type) {
            this.input = input;
            this.type = type;
            this.errors = [];
        }

        addError(message) {
            this.errors.push(message);
        }

        getMessages() {
            const status = this.input.validity;

            if (status.valueMissing) {
                this.addError("This field is required.");
            }
            if (status.typeMismatch) {
                if (this.type == "email") {
                    this.addError("Please enter a valid email address.");
                } else {
                    this.addError("Entry does not match the field type.");
                }
            }

            return this.errors;
        }
    }

    submit.addEventListener("click", (event) => {
        event.preventDefault(); //stop standard form submission.

        var errorCount = 0;

        //Remove all errors that exist in the form.
        var form = document.querySelector("form");
        var errors = document.querySelectorAll("form .error");
        while (errors.length > 0) {
            var error = document.querySelector("form .error");
            form.removeChild(error);

            errors = document.querySelectorAll("form .error");
        }

        let validateName = new CheckValidity(nameField, "text");
        let nameErrors = validateName.getMessages();

        if (nameErrors.length > 0) {
            nameErrors.forEach((err) => {
                nameField.insertAdjacentHTML('afterend', '<p class="error">' + err + '</p>');
                errorCount++;
            });
        }

        let validateEmail = new CheckValidity(emailField, "email");
        let emailErrors = validateEmail.getMessages();

        if (emailErrors.length > 0) {
            emailErrors.forEach((err) => {
                emailField.insertAdjacentHTML('afterend', '<p class="error">' + err + '</p>');
                errorCount++;
            });
        }

        //if error count is 0, submit form
        if (errorCount === 0) {
            const section = document.querySelector("#formdiv");
            section.innerHTML = '<h2>First Steps</h2><div id="registerMessage"><p id="registered">Awesome! We\'ll contact you within 1 business day.</p></div>';
        }
    });
}